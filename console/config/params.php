<?php
return [
    'adminEmail' => 'admin@example.com',
    'yii.migrations'=> \yii\helpers\ArrayHelper::merge([
        '@vendor/notgosu/yii2-meta-tag-module/src/migrations',
        '@vendor/vadymsemeniuk/yii2-file-processor-module/src/migrations',
        '@vendor/yiisoft/yii2/rbac/migrations',
        '@vendor/rmrevin/yii2-postman/migrations',
    ],
        require(__DIR__ . '/core_modules/migration-paths.php')
    )
];
