<?php

use yii\db\Schema;
use console\components\Migration;

/**
 * Class m151210_133440_create_redirects_table migration
 */
class m151210_133440_create_redirects_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%redirects}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'from' => $this->string()->notNull()->comment('From'),
                'to' => $this->string()->notNull()->comment('To'),
                'is_active' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1)->comment('Is active'),
                'created_at' => $this->integer()->notNull()->comment('Created at'),
                'updated_at' => $this->integer()->notNull()->comment('Updated at'),
            ],
            $this->tableOptions
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
