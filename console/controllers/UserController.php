<?php
namespace console\controllers;

use common\models\User;
use yii\console\Controller;
use yii\db\Query;
use yii\helpers\Console;

/**
 * Class UserController
 *
 * @package console\controllers
 */
class UserController extends Controller
{
    /**
     * @var string
     */
    public $defaultAction = 'check';

    /**
     * @param int $timeout
     */
    public function actionCheck($timeout = 86400)
    {
        $time = time() - $timeout;

        /** @var Query $query */
        $query = User::find();

        $query->andWhere(['status' => User::STATUS_SUSPENDED]);
        $query->andWhere(['<=', 'block_at', $time]);

        /** @var User[] $models */
        $models = $query->all();

        foreach ($models as $model) {
            if ($model->updateAttributes(['status' => User::STATUS_ACTIVE, 'block_at' => null])) {
                $this->stdout("User {$model->email} successfully unlocked.\n", Console::FG_GREEN);
            } else {
                $this->stdout("Error unblocking user {$model->email}.\n", Console::FG_RED);
            }
        }
    }

    /**
     * @param $email
     */
    public function actionBlock($email)
    {
        $user = User::findOne(['email' => $email]);

        if ($user) {
            if ($user->updateAttributes(['status' => User::STATUS_SUSPENDED, 'block_at' => time()])) {
                $this->stdout("User successfully locked.\n", Console::FG_GREEN);
            } else {
                $this->stdout("Error blocking user.\n", Console::FG_RED);
            }
        } else {
            $this->stdout("User not found.\n", Console::FG_RED);
        }
    }

    /**
     * @param $email
     */
    public function actionUnBlock($email)
    {
        $user = User::findOne(['email' => $email]);

        if ($user) {
            if ($user->updateAttributes(['status' => User::STATUS_ACTIVE, 'block_at' => null])) {
                $this->stdout("User successfully unlocked.\n", Console::FG_GREEN);
            } else {
                $this->stdout("Error unblocking user.\n", Console::FG_RED);
            }
        } else {
            $this->stdout("User not found.\n", Console::FG_RED);
        }
    }
}
