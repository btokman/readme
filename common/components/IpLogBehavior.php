<?php
namespace common\components;

use common\models\IpAuthLog;
use common\models\IpBlock;
use yii\base\Behavior;
use yii\helpers\ArrayHelper;

/**
 * Class IpLogBehavior
 *
 * @package backend\modules\admin\components
 */
class IpLogBehavior extends Behavior
{
    const ERROR_NONE = 'auth';
    const ERROR_SOME = 'some';

    /**
     * @return bool
     */
    public function logIp()
    {
        $log = new IpAuthLog();
        $log->date = time();
        $log->ip = $_SERVER['REMOTE_ADDR'];
        $log->host = @gethostbyaddr($_SERVER['REMOTE_ADDR']);
        $log->error = self::ERROR_NONE;
        return $log->save();
    }

    public function logIpError()
    {
        $log = new IpAuthLog();
        $log->date = time();
        $log->ip = $_SERVER['REMOTE_ADDR'];
        $log->host = @gethostbyaddr($_SERVER['REMOTE_ADDR']);
        $log->error = self::ERROR_SOME;
        return $log->save();
    }

    /**
     * @param int $limit
     *
     * @return bool
     */
    public function blockIpSequence($limit)
    {
        $models = IpAuthLog::find()
            ->where(['ip' => $_SERVER['REMOTE_ADDR']])
            ->orderBy(['date' => SORT_DESC])
            ->limit($limit)
            ->asArray()
            ->all();

        $list = ArrayHelper::map($models, 'id', 'error');

        if (count($models) < $limit) {
            return false;
        }

        if (in_array(self::ERROR_NONE, $list)) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function blockIp()
    {
        $block = new IpBlock();
        $block->date = time();
        $block->ip = $_SERVER['REMOTE_ADDR'];
        $block->host = @gethostbyaddr($_SERVER['REMOTE_ADDR']);
        if ($block->save()) {
            IpAuthLog::deleteAll(['ip' => $_SERVER['REMOTE_ADDR']]);

            return true;
        }

        return false;
    }
}
