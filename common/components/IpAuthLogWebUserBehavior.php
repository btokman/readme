<?php
namespace common\components;

use yii2tech\authlog\AuthLogIdentityBehavior;
use yii2tech\authlog\AuthLogWebUserBehavior;

/**
 * Class IpAuthLogWebUserBehavior
 *
 * @package backend\modules\admin\components
 */
class IpAuthLogWebUserBehavior extends AuthLogWebUserBehavior
{
    public function afterLogin($event)
    {
        /* @var $identity \yii\web\IdentityInterface|AuthLogIdentityBehavior|IpLogBehavior */
        $identity = $event->identity;

        $identity->logIp();

        parent::afterLogin($event);
    }
}
