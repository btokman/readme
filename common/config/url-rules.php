<?php
return [
    '' => 'site/index',
    'robots.txt' => 'site/robots',
    'sitemap.xml' => 'sitemap/default/index',
];
