<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%ip_auth_log}}".
 *
 * @property integer $id
 * @property integer $date
 * @property string $error
 * @property string $ip
 * @property string $host
 */
class IpAuthLog extends \common\components\model\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ip_auth_log}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'date' => Yii::t('app', 'Date'),
            'error' => Yii::t('app', 'Error'),
            'ip' => Yii::t('app', 'Ip'),
            'host' => Yii::t('app', 'Host'),
        ];
    }
}
