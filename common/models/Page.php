<?php

namespace common\models;

use common\components\ConfigurationComponent;
use common\models\base\StaticPage;
use Yii;

/**
 * This is class for static page model.
 */
class Page extends StaticPage
{
    const STRING = 'pageString';


    public $string;

    /**
    * @return array
    */
    public function getKeys()
    {
        return [
            self::STRING,
        ];
    }

    /**
    * @return $this
    */
    public function get()
    {
        /** @var ConfigurationComponent $config */
        $config = Yii::$app->config;
        $this->string = $config->get(self::STRING);

        return $this;
    }
}
