<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%ip_block}}".
 *
 * @property integer $id
 * @property integer $date
 * @property string $ip
 * @property string $host
 */
class IpBlock extends \common\components\model\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ip_block}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'date' => Yii::t('app', 'Date'),
            'ip' => Yii::t('app', 'Ip'),
            'host' => Yii::t('app', 'Host'),
        ];
    }
}
