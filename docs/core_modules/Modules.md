Готовые модули
==============

1. [vacancy](modules/vacancy.md) - Модуль вакансий.
2. [comment](modules/comment.md) - Модуль комментариев.
3. [currency](modules/currency.md) - Модуль валют.
