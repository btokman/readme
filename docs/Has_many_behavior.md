### Behavior сохранения multiple dropdown
Когда нужно сохранить выбраные значения multiple dropdown - удобно воспользоватся https://github.com/vadimsemenykv/yii2-many-to-many-ar-level-save-behavior
Документация довольно хорошо описана на странице github, но вкратце повторю здесь.

Предположим есть таблица меню ресторана Menu, таблица блюда Dish и таблица связи DishInMenu. Меню может содержать много блюд.

Все что нужно - в модели Menu обьявить переменную dishIds, добавить в rules правило safe для обьявленной переменной, 
добавить конфиг behavior и описать реляцию. Остальное бихевиор зделает сам.

````
class Menu extends ActiveRecord {
    public $dishIds = [];

    public function tableName() {
        return '{{%menu}}';
    }

    public function getDishes()
    {
        return $this->hasMany(Dish::className(), ['id' => 'dish_id'])
            ->viaTable('dish_in_menu', ['menu_id' => 'id']);
    }

    public function behaviors()
    {
        return [
            'manyToManyBehavior' => [
                'class' => ManyToManyArLevelSaveBehavior::className(),
                'relations' => [
                    'dishes' => [
                        'modelClass' => Dish::className(),
                        'attribute' => 'dishIds',
                        'pkColumnName' => 'id',
                        'deleteAllRelatedEntriesBeforeSave' => true,
                    ],
                ],
            ],
        ];
    }
}
````

Для специфических потребностей - можно указать дополнительные параметры реляций. 
Предположим, в таблице DishInMenu помимо столбиков 'id', 'menu_id', 'dish_id' добавился столбик 'type_id' для 
идентификации завтрака\обеда\ужина. Тогда это будет выглядеть примерно таким образом:

````
class Menu extends ActiveRecord {
    public $breakfastDishIds = [];
    public $dinnerDishIds = [];

    public function tableName() {
        return '{{%menu}}';
    }

    public function getBreakfastDishes()
    {
        return $this->hasMany(Dish::className(), ['id' => 'dish_id'])
            ->viaTable('dish_in_menu', ['menu_id' => 'id'], function(\yii\db\ActiveQuery $query) {
                return $query->andOnCondition(DishInMenu::tableName() . '.type_id = ' . DishInMenu::TYPE_BREAKFAST);
            });
    }
    
    public function getDinnerDishes()
    {
        return $this->hasMany(Dish::className(), ['id' => 'dish_id'])
            ->viaTable('dish_in_menu', ['menu_id' => 'id'], function(\yii\db\ActiveQuery $query) {
                return $query->andOnCondition(DishInMenu::tableName() . '.type_id = ' . DishInMenu::TYPE_DINNER);
            });
    }

    public function behaviors()
    {
        return [
            'manyToManyBehavior' => [
                'class' => ManyToManyArLevelSaveBehavior::className(),
                'relations' => [
                    'breakfastDishes' => [
                        'modelClass' => Dish::className(),
                        'attribute' => 'breakfastDishIds',
                        'pkColumnName' => 'id',
                        'deleteAllRelatedEntriesBeforeSave' => true,
                        'extraColumns' => [
                            'type_id' => DishInMenu::TYPE_BREAKFAST // <- параметры для сохранения помимо значений multiple dropdown
                        ],
                        'additionalUnlinkCondition' => [
                            'type_id' => DishInMenu::TYPE_BREAKFAST, // <- доп. параметры для запроса удаления
                        ]
                    ],
                    'dinnerDishes' => [
                        'modelClass' => Dish::className(),
                        'attribute' => 'dinnerDishIds',
                        'pkColumnName' => 'id',
                        'deleteAllRelatedEntriesBeforeSave' => true,
                        'extraColumns' => [
                            'type_id' => DishInMenu::TYPE_DINNER
                        ],
                        'additionalUnlinkCondition' => [
                            'type_id' => DishInMenu::TYPE_DINNER,
                        ]
                    ],
                ],
            ],
        ];
    }
}
````