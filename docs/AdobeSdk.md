### Работа с Adobe Creative SDK

ImageUpload виджет позволяет использовать Adobe Creative SDK API для редактирования изображений.

API дает возможность редактировать до 1 миллиона изображений в месяц, на один API ключ.

Для использования, в виджете нужно поставить свойство adobeSdk в true (по умолчанию false) :
 
```php
'image' => [
    'type' => ActiveFormBuilder::INPUT_RAW,
    'value' => ImageUpload::widget([
        'model' => $this,
        'attribute' => 'image',
        'adobeSdk' => true      <-------------------
        'saveAttribute' => EntityToFile::TYPE_SOME_IMAGE,
    ])
],
```

После чего появится возможность редактировать изображения с помощью SDK.

![adobe](../docs/img/adobe.jpg)


Редактор имеет множество возможностей (ресайз/кроп/различные эффекты)
 
![adobeExample](../docs/img/adobeExample.jpg)


Ключ к API хранится в  backend\modules\imagesUpload\widgets\imagesUpload

```php
class ImageUpload extends Widget {

const API_KEY = '52c9d6c4b30f40388493a4abdba25e3a';
```
 
[Инструкция по подключению API](https://docs.google.com/document/d/1VCWtbXsE_OwdKiyVxBV67Fq7JSluB4l7XZTGdQ7wHMs/edit)