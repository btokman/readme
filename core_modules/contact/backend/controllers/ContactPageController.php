<?php

namespace backend\modules\contact\controllers;

use backend\modules\configuration\components\ConfigurationController;
use backend\modules\contact\models\ContactPage;

/**
 * ContactPageController implements the CRUD actions for ContactPage model.
 */
class ContactPageController extends ConfigurationController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return ContactPage::className();
    }
}
