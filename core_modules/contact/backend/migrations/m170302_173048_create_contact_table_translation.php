<?php

use console\components\Migration;

/**
 * Class m170302_173048_create_contact_table_translation migration
 */
class m170302_173048_create_contact_table_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%contact_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%contact}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'label' => $this->string()->comment('Label'),
                'address' => $this->text()->comment('Address'),
            ],
            $this->tableOptions
        );

        
        $this->addPrimaryKey('pk-contact_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-contact_translation-model_id-contact-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}

