<?php

namespace backend\modules\contact\models;

use Yii;

/**
* This is the model class for table "{{%contact_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
* @property string $address
*/
class ContactTranslation extends \yii\db\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%contact_translation}}';
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'model_id' => Yii::t('back/contact', 'Related model id') . ' [' . $this->language . ']',
            'language' => Yii::t('back/contact', 'Language') . ' [' . $this->language . ']',
            'label' => Yii::t('back/contact', 'Label') . ' [' . $this->language . ']',
            'address' => Yii::t('back/contact', 'Address') . ' [' . $this->language . ']',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['label', 'address'], 'string', 'max' => 255],
         ];
    }
}
