<?php

/**
 * @var $this \yii\web\View
 * @var $page \common\models\ContactPage
 * @var $models \frontend\modules\contact\models\Contact[]
 */
use yii\bootstrap\Tabs;

?>

<h1><?= $page->label; ?></h1>

<p><?= nl2br($page->content); ?></p>

<?php
$items = [];
foreach ($models as $model) {
    $items[] = [
        'label' => $model->label,
        'content' => $this->render('_contact', ['model' => $model])
    ];
}
?>

<?= Tabs::widget(['items' => $items]); ?>
