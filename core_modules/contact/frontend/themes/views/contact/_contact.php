<?php

/**
 * @var $model \frontend\modules\contact\models\Contact
 */
?>

<?= nl2br($model->address); ?>
<br>
<a href="tel:<?= $model->phone; ?>"><?= $model->phone; ?></a>
<br>
<a href="mailto:<?= $model->email; ?>"><?= $model->email; ?></a>
