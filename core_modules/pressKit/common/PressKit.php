<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%press_kit}}".
 *
 * @property integer $id
 * @property EntityToFile $file
 */
class PressKit extends ActiveRecord 
{
    const SAVE_ATTRIBUTE_FILE = 'PressKitFile';

    const EXTENSIONS_IMAGE = ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'tiff', 'svg', 'exif'];
    const EXTENSION_PDF = 'pdf';
    const EXTENSIONS_VIDEO = ['mp4', 'webm', 'mov', 'ogv', 'ogg', 'mkv'];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%press_kit}}';
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getFile()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['file.entity_model_name' => static::formName(), 'file.attribute' => static::SAVE_ATTRIBUTE_FILE])
            ->alias('file')
            ->orderBy('file.position DESC');
    }

    public static function findFile($filename, $extension)
    {
        $model = FpmFile::find()
            ->alias('ff')
            ->joinWith('entityToFiles etf')
            ->where([
                'ff.base_name' => $filename,
                'ff.extension' => $extension,
                'etf.attribute' => static::SAVE_ATTRIBUTE_FILE
            ])
            ->orderBy(['ff.created_at' => SORT_DESC])
            ->one();
        return $model ? $model : false;
    }
}
