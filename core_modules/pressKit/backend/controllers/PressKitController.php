<?php

namespace backend\modules\pressKit\controllers;

use backend\components\BackendController;
use backend\modules\pressKit\models\PressKit;

/**
 * PressKitController implements the CRUD actions for PressKit model.
 */
class PressKitController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return PressKit::className();
    }
}
