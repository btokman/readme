<?php

return [
    'label' => Yii::t('back/comment', 'comment'),
    'items' => [
        [
            'label' => Yii::t('back/comment', 'comment'),
            'url' => ['/comment/comment/index'],
        ],
    ]
];
