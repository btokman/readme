<?php

namespace backend\modules\comment;

use common\components\interfaces\CoreModuleBackendInterface;

class Module extends \yii\base\Module implements CoreModuleBackendInterface
{
    public $controllerNamespace = 'backend\modules\comment\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

}
