<?php
namespace backend\modules\map\widgets\mapSelector;

use Yii;
use yii\web\AssetBundle;

/**
 * Class MapAsset
 *
 * @package backend\modules\map\widgets\mapSelector
 */
class MapAsset extends AssetBundle
{
    /**
     * @var string Google Developers API key
     * @link https://developers.google.com/maps/documentation/javascript/get-api-key
     */
    public $apiKey = 'AIzaSyAltkGUYQHsiMT4n6N4jSTfw2mDxP2i_6g';

    /**
     * @var string app language code to display map
     */
    public $language;

    /**
     * @inheritdoc
     */
    public $sourcePath = __DIR__;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->language = Yii::$app->language;
        $this->js = [
            "https://maps.googleapis.com/maps/api/js?key={$this->apiKey}&language={$this->language}&libraries=places",
            'assets/gmaps.js',
        ];
    }

    /**
     * @inheritdoc
     */
    public $depends = [
        'backend\assets\AppAsset'
    ];
}
