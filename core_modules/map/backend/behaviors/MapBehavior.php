<?php
namespace backend\modules\map\behaviors;

use Yii;
use yii\base\Behavior;
use common\components\model\ActiveRecord;
use backend\components\Model;
use backend\modules\configuration\components\ConfigurationModel;
use backend\modules\map\models\MapEntity;

/**
 * Class MapBehavior
 *
 * @property Model $owner the owner of this behavior.
 *
 * @package backend\modules\map\behaviors
 */
class MapBehavior extends Behavior
{

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
            ActiveRecord::EVENT_BEFORE_DELETE => 'beforeDelete',
            ConfigurationModel::EVENT_AFTER_SAVE => 'afterSave',
        ];
    }

    /**
     * After save event
     */
    public function afterSave()
    {
        $this->saveEntity();
    }

    /**
     * Before delete model event
     */
    public function beforeDelete()
    {
        $this->deleteEntity();
    }

    /**
     * Save map data to DB
     */
    private function saveEntity()
    {
        $model = $this->getEntityModel();
        if ($model->load(Yii::$app->getRequest()->post())) {
            if (!$model->save()) {
                Yii::$app->getSession()->setFlash('danger', Yii::t('back/map', 'Map saving error'));
            }
        }
    }

    /**
     * @return MapEntity
     */
    private function getEntityModel()
    {
        $entityModel =  MapEntity::findOne([
            'entity_model_name' => $this->owner->formName(),
            'entity_model_id' => $this->owner->id,
        ]);
        if ($entityModel === null) {
            $entityModel = new MapEntity([
                'entity_model_name' => $this->owner->formName(),
                'entity_model_id' => $this->owner->id,
            ]);
        }
        return $entityModel;
    }

    /**
     * Delete map data from Entity table
     */
    private function deleteEntity()
    {
        MapEntity::deleteAll([
            'entity_model_name' => $this->owner->formName(),
            'entity_model_id' => $this->owner->id,
        ]);
    }
}
