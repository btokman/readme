<?php
namespace backend\modules\map;

use common\components\interfaces\CoreModuleBackendInterface;

/**
 * Class Module
 *
 * @package backend\modules\map
 */
class Module extends \yii\base\Module implements CoreModuleBackendInterface
{

    /**
     * @var string
     */
    public $controllerNamespace = 'backend\modules\map\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }
}
