<?php
namespace frontend\modules\currency;

use common\components\interfaces\CoreModuleFrontendInterface;

/**
 * Class Module
 *
 * @package frontend\modules\currency
 */
class Module extends \yii\base\Module implements CoreModuleFrontendInterface
{

    /**
     * @inheritdoc
     */
    public function init()
    {
        return parent::init();
    }
}
