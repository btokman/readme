<?php
namespace backend\modules\currency;

use common\components\interfaces\CoreModuleBackendInterface;

/**
 * Class Module
 *
 * @package backend\modules\currency
 */
class Module extends \yii\base\Module implements CoreModuleBackendInterface
{
    /**
     * @var string
     */
    public $controllerNamespace = 'backend\modules\currency\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        return parent::init();
    }
}
