<?php

namespace frontend\modules\vacancy;

use common\components\interfaces\CoreModuleFrontendInterface;

/**
 * vacancy module definition class
 */
class Module extends \yii\base\Module implements CoreModuleFrontendInterface
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\vacancy\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

}
