<?php
/**
 * Created by anatolii
 *
 * @var $model VacancyRequestForm
 * @var $vacancy Vacancy
 */
use common\models\Vacancy;
use frontend\modules\vacancy\models\VacancyRequestForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<?php $form = ActiveForm::begin([
    'action' => Vacancy::getVacancyRequestUrl(['id' => $model->vacancy_id]),
    'options' => [
        'id' => 'vacancy-request-form',
        'class' => 'form-container ajax-form',
        'enctype' => 'multipart/form-data'
    ]
]); ?>
<div class="form-title">
    <h2><?= Yii::t('vacancy_request_form', 'title') ?></h2>
</div>

<?php $options = ['class' => 'form-field'] ?>
<?= $form->field($model, 'name')->textInput() ?>
<br/>
<?= $form->field($model, 'email')->textInput() ?>
<br/>
<?= $form->field($model, 'content')->textarea(['rows' => 2]) ?>
<div class="form-title"><?= $model->getAttributeLabel('file_id') ?></div>
<?= Html::activeFileInput($model, 'file_id') ?>
<?= Html::error($model, 'file_id') ?>
<?= Html::submitButton() ?>

<?php ActiveForm::end() ?>
