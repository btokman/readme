<?php

use console\components\Migration;

/**
 * Class m170124_093443_create_vacancy_request_table migration
 */
class m170124_093443_create_vacancy_request_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%vacancy_request}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'name' => $this->string()->notNull()->comment('Name'),
                'email' => $this->string()->notNull()->comment('Email'),
                'content' => $this->text()->null()->comment('Content'),
                'file_id' => $this->integer()->null()->comment('File'),
                'vacancy_id' => $this->integer()->null()->comment('Vacancy'),
                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(
            'fk-vacancy_request-vacancy_id-vacancy-id',
            $this->tableName,
            'vacancy_id',
            '{{%vacancy}}',
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-vacancy_request-vacancy_id-vacancy-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
