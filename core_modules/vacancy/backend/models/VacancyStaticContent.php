<?php

namespace backend\modules\vacancy\models;

use backend\modules\configuration\components\ConfigurationModel;
use common\models\Configuration;
use common\models\VacancyStaticContent as CommonVacancyStaticContent;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use metalguardian\formBuilder\ActiveFormBuilder;
/**
* Class VacancyStaticContent*/
class VacancyStaticContent extends ConfigurationModel
{
    /**
    * Attribute for imageUploader
    */
    public $images;

    /**
    * Title of the form
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('back/vacancy_static_content', 'Vacancy static content');
    }

    /**
    * @return array
    */
    public function getFormRules()
    {
        return [
            [CommonVacancyStaticContent::TITLE, 'string'],
            [CommonVacancyStaticContent::CONTENT, 'string'],
        ];
    }

    /**
    * @return array
    */
    public function getFormTypes()
    {
        return [
            CommonVacancyStaticContent::TITLE => Configuration::TYPE_STRING,
            CommonVacancyStaticContent::CONTENT => Configuration::TYPE_HTML,
        ];
    }

    /**
    * @return array
    */
    public function getFormDescriptions()
    {
        return [
            CommonVacancyStaticContent::TITLE => \Yii::t('back/vacancy_static_content', 'Title'),
            CommonVacancyStaticContent::CONTENT => \Yii::t('back/vacancy_static_content', 'Content'),
            'images' => \Yii::t('back/vacancy_static_content', 'Images'),
        ];
    }

    /**
    * @return array
    */
    public function getFormHints()
    {
        return [
            CommonVacancyStaticContent::TITLE => '',
            CommonVacancyStaticContent::CONTENT => '',
        ];
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            CommonVacancyStaticContent::TITLE,
            CommonVacancyStaticContent::CONTENT,
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
            ],
        ]);
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'form-set' => [
                'Main' => [
                    CommonVacancyStaticContent::TITLE,
                    CommonVacancyStaticContent::CONTENT,
                    'images' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'attribute' => 'images',
                            'saveAttribute' => CommonVacancyStaticContent::SAVE_ATTRIBUTE_IMAGES,
                            'aspectRatio' => false, //Пропорция для кропа
                            'multiple' => true, //Вкл/выкл множественную загрузку
                        ])
                    ],
                ],
            ]
        ];

        return $config;
    }
}
