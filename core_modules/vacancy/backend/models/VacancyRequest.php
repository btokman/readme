<?php

namespace backend\modules\vacancy\models;

use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%vacancy_request}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $content
 * @property integer $file_id
 * @property integer $vacancy_id
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Vacancy $vacancy
 */
class VacancyRequest extends ActiveRecord implements BackendModel
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%vacancy_request}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email'], 'required'],
            [['content'], 'string'],
            [['file_id', 'vacancy_id', 'published', 'position'], 'integer'],
            [['name', 'email'], 'string', 'max' => 255],
            [['vacancy_id'], 'exist', 'targetClass' => \common\models\Vacancy::className(), 'targetAttribute' => 'id'],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/vacancy_request', 'ID'),
            'name' => Yii::t('back/vacancy_request', 'Name'),
            'email' => Yii::t('back/vacancy_request', 'Email'),
            'content' => Yii::t('back/vacancy_request', 'Content'),
            'file_id' => Yii::t('back/vacancy_request', 'File'),
            'vacancy_id' => Yii::t('back/vacancy_request', 'Vacancy'),
            'published' => Yii::t('back/vacancy_request', 'Published'),
            'position' => Yii::t('back/vacancy_request', 'Position'),
            'created_at' => Yii::t('back/vacancy_request', 'Created At'),
            'updated_at' => Yii::t('back/vacancy_request', 'Updated At'),
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVacancy()
    {
        return $this->hasOne(Vacancy::className(), ['id' => 'vacancy_id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('back/vacancy_request', 'Vacancy Request');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'name',
                    'email:email',
                    // 'content:ntext',
                    'file_id:file',
                    [
                        'attribute' => 'vacancy_id',
                        'filter' => Vacancy::getItems(),
                        'format' => 'raw',
                        'value' => function ($data) {
                            return ArrayHelper::getValue($data->vacancy, 'label', Yii::t('back/vacancy', 'Vacancy not selected'));
                        }
                    ],
                    'published:boolean',
                    'position',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
                break;
            case 'view':
                return [
                    'id',
                    'name',
                    'email:email',
                    [
                        'attribute' => 'content',
                        'format' => 'html',
                    ],
                    'file_id:file',
                    [
                        'attribute' => 'vacancy_id',
                        'format' => 'raw',
                        'value' => ArrayHelper::getValue($this->vacancy, 'label', Yii::t('back/vacancy', 'Vacancy not selected'))
                    ],
                    'published:boolean',
                    'position',
                ];
                break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new VacancyRequestSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'name' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    
                ],
            ],
            'email' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    
                ],
            ],
            'content' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => \backend\components\ImperaviContent::className(),
                'options' => [
                    'model' => $this,
                    'attribute' => 'content',
                ]
            ],
            'file_id' => [
                'type' => ActiveFormBuilder::INPUT_FILE,
            ],
            'vacancy_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\Vacancy::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
            'position' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
        ];

        return $config;
    }


}
