<?php

namespace backend\modules\vacancy\controllers;

use backend\modules\configuration\components\ConfigurationController;
use backend\modules\vacancy\models\VacancyStaticContent;

/**
 * VacancyStaticContentController implements the CRUD actions for VacancyStaticContent model.
 */
class VacancyStaticContentController extends ConfigurationController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return VacancyStaticContent::className();
    }
}
