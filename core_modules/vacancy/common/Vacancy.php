<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;
use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;

/**
 * This is the model class for table "{{%vacancy}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $description
 * @property string $responsibilities
 * @property string $qualifications
 * @property integer $category_id
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property VacancyCategory $category
 * @property VacancyTranslation[] $translations
 * @property EntityToFile[] $images
 */
class Vacancy extends ActiveRecord implements Translateable
{
    use TranslateableTrait;
    use Translate;

    const SAVE_ATTRIBUTE_IMAGES = 'VacancyImages';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%vacancy}}';
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'label', 'description', 'responsibilities', 'qualifications'
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(VacancyCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVacancyRequests()
    {
        return $this->hasMany(VacancyRequest::className(), ['vacancy_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(VacancyTranslation::className(), ['model_id' => 'id']);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getImages()
    {
        return $this->hasMany(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['images.entity_model_name' => static::formName(), 'images.attribute' => static::SAVE_ATTRIBUTE_IMAGES])
            ->alias('images')
            ->orderBy('images.position DESC');
    }

    /**
     * @return string
     */
    public function getVacancyPageUrl()
    {
        return static::createUrl('/vacancy/default/vacancy-page', ['id' => $this->id]);
    }

    /**
     * @param array $params
     *
     * @return string
     */
    public static function getVacancyRequestUrl(array $params = [])
    {
        return static::createUrl('/vacancy/default/vacancy-request', $params);
    }
}
