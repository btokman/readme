<?php

namespace backend\modules\news\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * NewsSearch represents the model behind the search form about `News`.
 */
class NewsSearch extends News
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'published', 'position', 'category_id'], 'integer'],
            [['label', 'alias', 'content', 'publish_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NewsSearch::find()
            ->alias('n')
            ->joinWith('newsCategories cat')
            ->orderBy(['n.publish_date' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'n.id' => $this->id,
            'n.published' => $this->published,
            'n.position' => $this->position,
        ]);

        if($this->publish_date)
        {
            $query->andFilterWhere(['>', 'n.publish_date', strtotime($this->publish_date)]);
            $query->andFilterWhere(['<', 'n.publish_date', strtotime($this->publish_date.'+1 day')]);
        }

        if($this->category_id)
        {
            $query->andWhere(['cat.id' => $this->category_id]);
        }

        $query->andFilterWhere(['like', 'n.label', $this->label])
            ->andFilterWhere(['like', 'n.alias', $this->alias])
            ->andFilterWhere(['like', 'n.content', $this->content]);

        return $dataProvider;
    }
}
