<?php

namespace backend\modules\news\models;

use Yii;

/**
* This is the model class for table "{{%news_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
* @property string $content
*/
class NewsTranslation extends \yii\db\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%news_translation}}';
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'model_id' => Yii::t('back/news', 'Related model id') . ' [' . $this->language . ']',
            'language' => Yii::t('back/news', 'Language') . ' [' . $this->language . ']',
            'label' => Yii::t('back/news', 'Label') . ' [' . $this->language . ']',
            'content' => Yii::t('back/news', 'Content') . ' [' . $this->language . ']',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['content'], 'string'],
            [['label'], 'string', 'max' => 255],
         ];
    }
}
