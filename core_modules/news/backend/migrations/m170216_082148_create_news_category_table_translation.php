<?php

use console\components\Migration;

/**
 * Class m170216_082148_create_news_category_table_translation migration
 */
class m170216_082148_create_news_category_table_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%news_category_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%news_category}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'label' => $this->string()->comment('Label'),
            ],
            $this->tableOptions
        );

        
        $this->addPrimaryKey('pk-news_category_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-news_category_translation-model_id-news_category-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}

