<?php

use console\components\Migration;

/**
 * Class m170216_083231_create_news_to_news_category_table migration
 */
class m170216_083231_create_news_to_news_category_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%news_to_news_category}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'news_id' => $this->integer()->notNull()->comment('News Id'),
                'category_id' => $this->integer()->notNull()->comment('Category Id'),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(
            'fk-news_to_news_category-news_id-news-id',
            $this->tableName,
            'news_id',
            '{{%news}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-news_to_news_category-category_id-news_category-id',
            $this->tableName,
            'category_id',
            '{{%news_category}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-news_to_news_category-news_id-news-id', $this->tableName);
        $this->dropForeignKey('fk-news_to_news_category-category_id-news_category-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
