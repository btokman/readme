<?php

namespace frontend\modules\news;

use common\components\interfaces\CoreModuleFrontendInterface;

class Module extends \yii\base\Module implements CoreModuleFrontendInterface
{
    public $controllerNamespace = 'frontend\modules\news\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
