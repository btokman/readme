<?php
use common\models\News;
/**
 * @author walter
 *
 * @var $models \common\models\NewsCategory[]
 */
?>
<ul>
    <li><a href="<?= News::getListPageUrl(); ?>">All</a></li>
    <?php foreach ($models as $model) { ?>
        <li><a href="<?= $model->getPageUrl(); ?>"><?= $model->label; ?></a></li>
    <?php } ?>
</ul>
