<?php
namespace frontend\controllers;

use common\models\Robots;
use frontend\components\FrontendController;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Site controller
 */
class SiteController extends FrontendController
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionRobots()
    {
        $robots = Robots::find()->one();

        if (!$robots) {
            throw new NotFoundHttpException();
        }

        $this->layout = false;

        \Yii::$app->response->format = Response::FORMAT_RAW;
        $headers = \Yii::$app->response->headers;
        $headers->add('Content-Type', 'text/plain');

        $text = $robots->text;

        $text .= "\nSitemap: " . Url::to(['/sitemap/default/index'], true);

        return $this->renderContent($text);
    }
}
