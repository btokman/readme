Melon.ng Change Log
===================

0.7.0 Mar 28, 2017
---------------------
- Add: contact module with map(roman_gonchar)
- Add: press-kit module(walter-007)
- Add: news module(walter-007)
- Add: currency module(roman_gonchar)
- Add: vacancy module(tolik505)
- Add: comment module(tolik505)
- Add: core modules functionality(tolik505)
- Add: User IP block and User auth log for backend(hanterrian)
- Add: Full backend module generator(tolik505)
- Add: Migration generator + models generator added(tolik505)
- Fix: Update migrations for PostgreSQL(dvixi)
- Fix: remove redirect to lowerCase for images from uploads(walter-007) 
- Fix: fixes backend url rules inheritance from common/front rules(dvixi)
- Fix: various bug fixes(dvixi, vitaliy_shkolin, sasha-myha, delagics, tolik505, walter-007)
- Enh: configurator module various enhancements(tolik505, walter-007)
- Chg: package's version update (vitaliy_shkolin, delagics, walter-007)

0.6.2 Sep 21, 2016
---------------------
- Add: "Configurator" module for easy creating entities backend forms
 without creating tables for each entity(tolik505)
- Add: ajax redirect handler in common.js(dvixi)
- Add: possibility to add custom seo text and noindex tag in seo-behavior(hanterrian)
- Fix: application is now PosgtreSQL ready(dvixi)
- Chg: update bower-asset/cropper package to latest version(dvixi)

0.6.1 May 23, 2016
---------------------
- Add: backend related form widget(tolik505)
- Add: backend form config tabs(tolik505)
- Add: Docs for new Gii(notgosu)
- Fix: small html bugs fixed(dvixi)
- Fix: change cropper viewMode to prevent crop outside of image(notgosu)
- Fix: clear modal window after close(notgosu)
- Chg: updated to latest Yii version
- Chg: switch to another main page tree widget(tolik505)

0.6.0beta April 19, 2016
------------------------
- Chg: Updated backend Gii CRUD, Model generators. Backend models now 
separated from common(notgosu)
- Enh: Use `common\components\Schema` to support `comment()` method inside
 migrations code(metal, notgosu)
