<?php

namespace backend\modules\pages\controllers;

use backend\modules\configuration\components\ConfigurationController;
use backend\modules\pages\models\Page;

/**
 * PageController implements the CRUD actions for Page model.
 */
class PageController extends ConfigurationController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Page::className();
    }
}
