<?php

namespace backend\modules\pages\models;

use backend\modules\configuration\components\ConfigurationModel;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use common\models\Configuration;
use common\models\Page as CommonPage;
use metalguardian\formBuilder\ActiveFormBuilder;

/**
* Class Page*/
class Page extends ConfigurationModel
{
    public $showAsConfig = false;

    public $image;

    public $sign;

    public function init()
    {
        parent::init();

        if (!$this->sign) {
            $this->sign = \Yii::$app->security->generateRandomString();
        }
    }
    /**
    * Title of the form
    *
    * @return string
    */
    public function getTitle()
    {
        return 'Page';
    }

    /**
    * @return array
    */
    public function getFormRules()
    {
        return [
            [CommonPage::STRING, 'required'],
        ];
    }

    /**
    * @return array
    */
    public function getFormTypes()
    {
        return [
            CommonPage::STRING => Configuration::TYPE_STRING,
        ];
    }

    /**
    * @return array
    */
    public function getFormDescriptions()
    {
        return [
            CommonPage::STRING => 'string',
        ];
    }

    /**
    * @return array
    */
    public function getFormHints()
    {
        return [
            CommonPage::STRING => '',
        ];
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
        ];
    }


    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'form-set' => [
                'Main' => [
                    CommonPage::STRING,
                    'image' => [
                        'type' => ActiveFormBuilder::INPUT_FILE,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'attribute' => 'image',
                            'saveAttribute' => 'bla',
                            'aspectRatio' => true,
                            'multiple' => false,
                            'adobeSdk' => true,
                            'allowedFileExtensions' => ['png', 'jpg'],
                        ])
                    ],
                ],
            ]
        ];

        return $config;
    }
}
