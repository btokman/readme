<?php
/**
 * @var $form FormBuilder
 * @var $model ConfigurationModel
 * @var $element string
 */
use backend\components\FormBuilder;
use common\components\model\Translateable;
use backend\modules\configuration\components\ConfigurationModel;
use backend\modules\configuration\models\Configurator;
use common\helpers\LanguageHelper;

/** @var Configurator[] $values */
$values = $model->getModels();

$content = null;
foreach ($values as $value) {
    if ($value->id == $element) {
        $language = $value instanceof Translateable ? '[language: ' . LanguageHelper::getCurrent()->code . ']' : null;
        $attribute = '[' . $value->id . ']value';
        $configuration = $value->getValueFieldConfig();
        $configuration['label'] = $model->showAsConfig ?
            $value->description . ' [key: ' . $value->id . ']' . $language
            : $value->description;
        $content .= $form->renderField($value, $attribute, $configuration);
        $content .= $form->renderUploadedFile($value, 'value', $configuration);
        if (!is_array($value->type) && $language && $model->isTranslateAttribute($value->id)) {
            foreach ($value->getTranslationModels() as $languageModel) {
                $configuration['label'] = $model->showAsConfig ?
                    $value->description . ' [key: ' . $value->id . '] [language: ' . $languageModel->language . ']' :
                    $value->description . ' [' . $languageModel->language . ']';
                $content .= $form->renderField($languageModel, '[' . $languageModel->language . ']' . $attribute,
                    $configuration);
                $content .= $form->renderUploadedFile($languageModel, 'value', $configuration,
                    $languageModel->language);
            }
        }
    }
}

echo $content;
