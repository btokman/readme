<?php

namespace backend\modules\admin\models;

use Yii;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;

/**
 * This is the model class for table "{{%ip_auth_log}}".
 *
 * @property integer $id
 * @property integer $date
 * @property string $error
 * @property string $ip
 * @property string $host
 */
class IpAuthLog extends \common\components\model\ActiveRecord implements BackendModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ip_auth_log}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'ip', 'host'], 'required'],
            [['date'], 'integer'],
            [['error', 'ip', 'host'], 'string', 'max' => 255],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'date' => Yii::t('app', 'Date'),
            'error' => Yii::t('app', 'Error'),
            'ip' => Yii::t('app', 'Ip'),
            'host' => Yii::t('app', 'Host'),
        ];
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return \Yii::t('app', 'Ip Auth Log');
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     *
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'date:datetime',
                    'error',
                    'ip',
                    'host',
                    //['class' => 'yii\grid\ActionColumn'],
                ];
                break;
            case 'view':
                return [
                    'id',
                    'date:datetime',
                    'error',
                    'ip',
                    'host',
                ];
                break;
        }

        return [];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new IpAuthLogSearch();
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        return [
            'date' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
            'error' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,

                ],
            ],
            'ip' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,

                ],
            ],
            'host' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,

                ],
            ],

        ];
    }

}
