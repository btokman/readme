<?php
/**
 * Author: hanterrian
 */

namespace backend\modules\admin\widgets\authLog;

use backend\modules\admin\models\User;
use common\models\UserAuthLog;
use yii\base\Widget;

/**
 * Class AuthLogWidget
 *
 * @package backend\modules\admin\widgets\authLog
 */
class AuthLogWidget extends Widget
{
    public $model;

    public $attribute;

    public $options = [];

    public function run()
    {
        /** @var User $model */
        $model = $this->model;

        /** @var UserAuthLog[]|array $value */
        $value = $model->{$this->attribute};

        echo $this->render('default', ['models' => $value]);
    }
}
