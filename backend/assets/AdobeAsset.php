<?php
namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Class AdobeAsset
 * @package backend\assets
 */
class AdobeAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'http://feather.aviary.com/imaging/v3/editor.js',
        YII_ENV_DEV ? 'js/creative_sdk.js' : 'js/creative_sdk.min.js'

    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
