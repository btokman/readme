<?php

return [
    require(__DIR__ . '/core_modules/menu-items.php'),
    [

        'label' => Yii::t('back/menu', 'settings'),
        'items' => [
            [
                'label' => Yii::t('back/menu', 'configurations'),
                'url' => ['/configuration/default/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'page_seo'),
                'url' => ['/seo/page-seo/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'internationalization'),
                'items' => [
                    [
                        'label' => Yii::t('back/menu', 'translations'),
                        'url' => ['/i18n/default/index'],
                    ],
                    [
                        'label' => Yii::t('back/menu', 'languages'),
                        'url' => ['/language/language/index'],
                    ],
                ],
            ],
        ],
    ],
    [
        'label' => 'User',
        'items' => [
            [
                'label' => Yii::t('app', 'User'),
                'url' => ['/admin/user/index'],
            ],
            [
                'label' => Yii::t('app', 'Ip Log'),
                'url' => ['/admin/ip-auth-log/index'],
            ],
            [
                'label' => Yii::t('app', 'Ip Block'),
                'url' => ['/admin/ip-block/index'],
            ],
        ]
    ],
    [
        'label' => 'test',
        'url' => ['/pages/page/update']
    ],

];
