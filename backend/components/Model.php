<?php

namespace backend\components;


use common\components\model\ActiveQuery;
use common\components\model\ActiveRecord;
use common\components\model\RelatedFormHelpTrait;
use yii\base\InvalidParamException;
use yii\base\UnknownMethodException;
use yii\db\ActiveQueryInterface;
use yii\db\ActiveRecordInterface;
use yii\db\Exception;
use yii\helpers\ArrayHelper;

/**
 * Class Model
 *
 * @package backend\components
 */
class Model extends \yii\base\Model
{
    use RelatedFormHelpTrait;

    /**
     * Creates and populates a set of models.
     *
     * @param string $modelClass
     * @param $post array
     * @param array $multipleModels
     *
     * @return array
     */
    public static function createMultiple($modelClass, $post, $multipleModels = [])
    {
        $model = new $modelClass;
        $formName = $model->formName();
        $post = isset($post[$formName]) ? $post[$formName] : null;
        $models = [];

        if (!empty($multipleModels)) {
            $keys = array_keys(ArrayHelper::map($multipleModels, 'id', 'id'));
            $multipleModels = array_combine($keys, $multipleModels);
        }

        if ($post && is_array($post)) {
            foreach ($post as $i => $item) {
                if (isset($item['id']) && !empty($item['id']) && isset($multipleModels[$item['id']])) {
                    $models[$i] = $multipleModels[$item['id']];
                } else {
                    $models[$i] = new $modelClass;
                }
            }
        }

        unset($model, $formName, $post);

        return $models;
    }

    /**
     * @param array $models
     * @param array $data
     * @param string | null $formName
     * @param int | null $index
     *
     * @return bool
     */
    public static function loadMultiple($models, $data, $formName = null, $index = null)
    {
        if ($formName === null) {
            /* @var $first Model */
            $first = reset($models);
            if ($first === false) {
                return false;
            }
            $formName = $first->formName();
        }
        $success = false;
        /* @var $models Model[] */
        foreach ($models as $i => $model) {
            if ($formName == '') {
                if (is_null($index)) {
                    $loadData = !empty($data[$i]) ? $data[$i] : [];
                } else {
                    $loadData = !empty($data[$index][$i]) ? $data[$index][$i] : [];
                }
            } else {
                if (is_null($index)) {
                    $loadData = !empty($data[$formName][$i]) ? $data[$formName][$i] : [];
                } else {
                    $loadData = !empty($data[$formName][$index][$i]) ? $data[$formName][$index][$i] : [];
                }
            }
            if (!empty($loadData)) {
                $model->load($loadData, '');
                $success = true;
            }
        }

        return $success;
    }

    /**
     * @param $model ActiveRecord
     * @param $relModels ActiveRecord[]
     * @param $config array
     * @param $key string
     * @param $deletedIDs array
     * @param $relatedModel string
     * @param $flag bool
     *
     * @throws Exception
     */
    public static function saveRelModels($model, $relModels, $config, $key, $deletedIDs, $relatedModel, &$flag)
    {
        if ($flag) {
            if (!empty($deletedIDs)) {
                $relatedModel::deleteAll(['id' => $deletedIDs]);
            }
            $i = 0;
            foreach ($relModels as $modelRel) {
                $relatedAttribute = key($model->getRelation($config[$key]['relation'])->link);
                $modelRel->$relatedAttribute = $model->id;
                if ($modelRel->hasAttribute('position')) {
                    $modelRel->position = $i++;
                }
                if (!($flag = $modelRel->save(false) && $flag)) {
                    break;
                }
            }
        }
    }

    /**
     * Declares a `has-many` relation.
     * The declaration is returned in terms of a relational [[ActiveQuery]] instance
     * through which the related record can be queried and retrieved back.
     *
     * A `has-many` relation means that there are multiple related records matching
     * the criteria set by this relation, e.g., a customer has many orders.
     *
     * For example, to declare the `orders` relation for `Customer` class, we can write
     * the following code in the `Customer` class:
     *
     * ```php
     * public function getOrders()
     * {
     *     return $this->hasMany(Order::className(), ['customer_id' => 'id']);
     * }
     * ```
     *
     * Note that in the above, the 'customer_id' key in the `$link` parameter refers to
     * an attribute name in the related class `Order`, while the 'id' value refers to
     * an attribute name in the current AR class.
     *
     * Call methods declared in [[ActiveQuery]] to further customize the relation.
     *
     * @param string $class the class name of the related record
     * @param array $link the primary-foreign key constraint. The keys of the array refer to
     * the attributes of the record associated with the `$class` model, while the values of the
     * array refer to the corresponding attributes in **this** AR class.
     * @return ActiveQueryInterface the relational query object.
     */
    public function hasMany($class, $link)
    {
        /* @var $class ActiveRecordInterface */
        /* @var $query ActiveQuery */
        $query = $class::find();
        $query->primaryModel = $this;
        $query->link = $link;
        $query->multiple = true;

        return $query;
    }

    /**
     * Returns the relation object with the specified name.
     * A relation is defined by a getter method which returns an [[ActiveQueryInterface]] object.
     * It can be declared in either the Active Record class itself or one of its behaviors.
     * @param string $name the relation name
     * @param boolean $throwException whether to throw exception if the relation does not exist.
     * @return ActiveQueryInterface|ActiveQuery the relational query object. If the relation does not exist
     * and `$throwException` is false, null will be returned.
     * @throws InvalidParamException if the named relation does not exist.
     */
    public function getRelation($name, $throwException = true)
    {
        $getter = 'get' . $name;
        try {
            // the relation could be defined in a behavior
            $relation = $this->$getter();
        } catch (UnknownMethodException $e) {
            if ($throwException) {
                throw new InvalidParamException(get_class($this) . ' has no relation named "' . $name . '".', 0, $e);
            } else {
                return null;
            }
        }
        if (!$relation instanceof ActiveQueryInterface) {
            if ($throwException) {
                throw new InvalidParamException(get_class($this) . ' has no relation named "' . $name . '".');
            } else {
                return null;
            }
        }

        if (method_exists($this, $getter)) {
            // relation name is case sensitive, trying to validate it when the relation is defined within this class
            $method = new \ReflectionMethod($this, $getter);
            $realName = lcfirst(substr($method->getName(), 3));
            if ($realName !== $name) {
                if ($throwException) {
                    throw new InvalidParamException('Relation names are case sensitive. ' . get_class($this) . " has a relation named \"$realName\" instead of \"$name\".");
                } else {
                    return null;
                }
            }
        }

        return $relation;
    }
}
