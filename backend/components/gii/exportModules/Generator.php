<?php
/**
 * Created by anatolii
 */

namespace backend\components\gii\exportModules;

use Yii;
use yii\gii\CodeFile;


/**
 * This generator generates model for static pages
 */
class Generator extends \yii\gii\generators\model\Generator
{
    public $coreModuleNames = [];
    public $selectedModules = [];

    public function init()
    {
        parent::init();

        $this->coreModuleNames = AppModule::getAppModuleIds();
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'Export modules';
    }

    /**
     * @inheritdoc
     */
    public function getDescription()
    {
        return 'This generator exports modules to core_modules';
    }

    /**
     * @inheritdoc
     */
    public function requiredTemplates()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['selectedModules'], 'safe'],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'selectedModules' => 'Modules',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function hints()
    {
        return array_merge(parent::hints(), [

        ]);
    }

    /**
     * @inheritdoc
     */
    public function generate()
    {
        $files = [];
        foreach ($this->selectedModules as $moduleId) {
            $appModule = new AppModule($moduleId);
            $filesForExport = $appModule->getFilesForExport();
            foreach ($filesForExport as $appModuleFile => $coreModuleFile) {
                $files[] = new CodeFile(
                    $coreModuleFile,
                    file_get_contents($appModuleFile)
                );
            }
        }

        return $files;
    }

}
