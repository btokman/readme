<?php
/**
 * Created by anatolii
 */

namespace backend\components\gii\exportModules;


use backend\components\gii\importModules\CoreModule;
use common\components\interfaces\CoreModuleBackendInterface;
use Yii;
use yii\base\ErrorException;
use yii\helpers\Inflector;

/**
 * Class AppModule
 *
 * @package backend\components\gii\exportModules
 */
class AppModule extends BaseModule
{
    /**
     * @var CoreModule
     */
    private $_coreModule;

    /**
     * AppModule constructor.
     *
     * @param $moduleId
     */
    public function __construct($moduleId)
    {
        parent::__construct($moduleId);
    }

    /**
     * @return CoreModule
     */
    protected function getCoreModule()
    {
        if (!$this->_coreModule) {
            $this->_coreModule = new CoreModule($this->id);
        }

        return $this->_coreModule;
    }

    /**
     * @return string
     */
    public function getBackendPath()
    {
        return Yii::getAlias('@backend') . '/modules/' . $this->id;
    }

    /**
     * @param string $moduleId
     *
     * @return string | CoreModuleBackendInterface
     */
    public static function getBackendModuleClassName($moduleId)
    {
        return "\\backend\\modules\\$moduleId\\Module";
    }

    /**
     * @return string
     */
    public function getFrontendPath()
    {
        return Yii::getAlias('@frontend') . '/modules/' . $this->id;
    }

    /**
     * @return string
     */
    public function getFrontendThemesPath()
    {
        return Yii::getAlias('@frontend') . '/themes/basic/modules/' . $this->id;
    }

    /**
     * @return string
     */
    public function getCommonPath()
    {
        return Yii::getAlias('@common') . '/models';
    }

    /**
     * @return array
     */
    public function getFilesForExport()
    {
        $coreModule = $this->getCoreModule();
        $files = [];
        foreach ($this->getFilesFromDirectory($this->getBackendPath()) as $item) {
            $array = explode("modules/$this->id", $item);
            if (isset($array[1])) {
                $files[$item] = $coreModule->getBackendPath() . $array[1];
            }
        }
        foreach ($this->getFilesFromDirectory($this->getFrontendPath()) as $item) {
            $array = explode("modules/$this->id", $item);
            if (isset($array[1])) {
                $files[$item] = $coreModule->getFrontendPath() . $array[1];
            }
        }
        foreach ($this->getFilesFromDirectory($this->getFrontendThemesPath()) as $item) {
            $array = explode("modules/$this->id", $item);
            if (isset($array[1])) {
                $files[$item] = $coreModule->getFrontendThemesPath() . $array[1];
            }
        }
        foreach ($this->getCommonModelPaths() as $modelName => $modelPath) {
            $files[$modelPath] = $coreModule->getCommonPath() . '/' . $modelName;
        }

        return $files;
    }

    public function remove()
    {
        try {
            $this->removeDirectory($this->getBackendPath());
            $this->removeDirectory($this->getFrontendPath());
            $this->removeDirectory($this->getFrontendThemesPath());
            foreach ($this->getCommonModelPaths() as $path) {
                unlink($path);
                $this->log[] = "$path was removed";
            }
            $this->log[] = "Module '$this->id' was removed successfully";
        } catch (ErrorException $exception) {
            $this->log[] = "An error has occurred while removing '$this->id' module";
            $this->log[] = $exception->getMessage();
            $this->log[] = $exception->getTraceAsString();
        }
    }

    /**
     * @return array
     */
    public static function getAppModuleIds()
    {
        $ids = [];
        $dirs = array_filter(glob(Yii::getAlias('@backend') . '/modules/*'), 'is_dir');
        foreach ($dirs as $item) {
            $array = explode('/modules/', $item);
            if (count($array) == 2) {
                $moduleId = $array[1];
                $backendModuleClassName = static::getBackendModuleClassName($moduleId);
                if (class_exists($backendModuleClassName)) {
                    $interfaces = class_implements($backendModuleClassName);
                    if (isset($interfaces[CoreModuleBackendInterface::class])) {
                        $ids[$moduleId] = $moduleId;
                    }
                }
            }
        }

        return $ids;
    }

    /**
     * @return array
     */
    protected function getCommonModelPaths()
    {
        $files = glob($this->getCommonPath() . '/' . Inflector::id2camel($this->id) . '*');
        $paths = [];
        foreach ($files as $file) {
            $fileName = $this->getCommonModelFileName($file);
            if ($fileName) {
                $paths[$fileName] = $file;
            }
        }

        return $paths;
    }

    /**
     * @param string $file
     *
     * @return string|null
     */
    protected function getCommonModelFileName($file)
    {
        $array = explode('/', $file);
        $count = count($array);
        if (substr_count($array[$count - 1], '.php')) {
            return $array[$count - 1];
        }

        return null;
    }
}
