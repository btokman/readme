<?php
/**
 * Created by anatolii
 */

namespace backend\components\gii\removeModules;

use backend\components\gii\exportModules\AppModule;
use Yii;
use yii\gii\CodeFile;
use yii\helpers\ArrayHelper;


/**
 * This generator generates model for static pages
 */
class Generator extends \backend\components\gii\importModules\Generator
{
    /**
     * @var array
     */
    public $log = [];

    public function init()
    {
        parent::init();

        $this->coreModuleNames = AppModule::getAppModuleIds();
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'Remove modules';
    }

    /**
     * @inheritdoc
     */
    public function getDescription()
    {
        return 'This generator removes modules from applications. Note! Files with action "unchanged" will be removed';
    }

    /** @inheritdoc */
    public function getTemplatePath()
    {
        return Yii::getAlias('@backend') . '/components/gii/importModules/default';

    }

    /**
     * @inheritdoc
     */
    public function generate()
    {
        foreach ($this->selectedModules as $moduleId) {
            $appModule = new AppModule($moduleId);
            $filesForRemove = $appModule->getFilesForExport();
            foreach ($filesForRemove as $appModuleFile => $coreModuleFile) {
                $files[] = new CodeFile(
                    $appModuleFile,
                    file_get_contents($appModuleFile)
                );
            }
        }
        $modulesAfterRemove = array_diff(AppModule::getAppModuleIds(), $this->selectedModules);

        return $this->getRenderedFiles($modulesAfterRemove, $files);
    }

    public function removeModules()
    {
        foreach ($this->selectedModules as $moduleId) {
            $appModule = new AppModule($moduleId);
            $appModule->remove();
            $this->log = ArrayHelper::merge($this->log, $appModule->log);
        }
    }
}
