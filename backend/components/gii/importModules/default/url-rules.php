<?php
/**
 * @var $modules array
 */

echo "<?php\n";

$count = count($modules);
switch (true) {
    case $count > 1:
?>
return \yii\helpers\ArrayHelper::merge(
<?php foreach ($modules as $key => $moduleId): ?>
    include(Yii::getAlias('@frontend') . '/modules/<?= $moduleId ?>/url-rules/url-rules.php')<?= $key + 1 != $count ? ',' : '' ?>

<?php endforeach; ?>
);
<?php
    break;
    case $count == 1: ?>
return include(Yii::getAlias('@frontend') . '/modules/<?= array_shift($modules) ?>/url-rules/url-rules.php');
<?php break;
    default:
?>
return [];
<?php } ?>
