<?php
/**
 * Created by PhpStorm.
 * User: anatolii
 * Date: 30.12.15
 * Time: 9:40
 */

namespace backend\components;


use backend\modules\configuration\models\Configuration;
use common\components\model\ActiveRecord;
use metalguardian\fileProcessor\helpers\FPM;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Class FormBuilder
 *
 * @package backend\components
 */
class FormBuilder extends ActiveFormBuilder
{
    /**
     * @param $attribute
     * @param array $element
     * @param ActiveRecord $model
     * @param string|null $language
     *
     * @return string
     */
    public function renderUploadedFile($model, $attribute, $element, $language = null)
    {
        $content = '';
        if ($element['type'] == static::INPUT_FILE && isset($model->$attribute) && $model->$attribute) {
            $file = FPM::transfer()->getData($model->$attribute);
            $content .= Html::beginTag('div', ['class' => 'file-name']);
            $content .= Html::button(\Yii::t('app', 'Delete file'), [
                'class' => 'delete-file',
                'data' => [
                    'modelName' => $model->className(),
                    'modelId' => $language ? $model->model_id : $model->id,
                    'attribute' => $attribute,
                    'language' => $language
                ]
            ]);
            $content .= Formatter::getFileLink($file);
            $content .= Html::endTag('div');
        }
        return $content;
    }

    /**
     * @param $model ActiveRecord
     * @param $formConfig array
     * @param $translationModels ActiveRecord[]
     * @param $tabs bool
     *
     * @return null|string
     */
    public function prepareRows($model, $formConfig, $translationModels, $tabs = false)
    {
        $content = null;
        foreach ($formConfig as $attribute => $element) {
            if (isset($element['relation']) && $model->relModels) {
                if ($tabs) {
                    $content .= $this->render('//templates/_related_form', [
                        'relModels' => $model->relModels[$element['relation']],
                        'form' => $this
                    ]);
                } else {
                    foreach ($model->relModels as $relModels) {
                        $content .= $this->render('//templates/_related_form', [
                            'relModels' => $relModels,
                            'form' => $this
                        ]);
                    }
                }
            } else {
                $view = !is_array($element)
                    ? '@app/modules/configuration/views/configuration/templates/_form_row'
                    : '//templates/_form_row';
                $content .= $this->render($view, [
                    'model' => $model,
                    'attribute' => $attribute,
                    'element' => $element,
                    'translationModels' => $translationModels,
                    'form' => $this
                ]);
            }
        }

        return $content;
    }

    /**
     * @inheritdoc
     */
    public function renderField(\yii\base\Model $model, $attribute, array $settings = [])
    {
        $label = ArrayHelper::getValue($settings, 'label');
        if ($model instanceof Configuration && is_array($model->type)) {
            $settings = $model->type;
            if ($label !== null) {
                $settings['label'] = $label;
            }
        }
        $fieldOptions = ArrayHelper::getValue($settings, 'fieldOptions', []);
        $field = $this->field($model, $attribute, $fieldOptions);

        if ($label !== null) {
            $field->label($label, ArrayHelper::getValue($settings, 'labelOptions', []));
        }
        if (($hint = ArrayHelper::getValue($settings, 'hint')) !== null) {
            $field->hint($hint, ArrayHelper::getValue($settings, 'hintOptions', []));
        }

        $type = ArrayHelper::getValue($settings, 'type', static::INPUT_TEXT);
        $this->prepareField($field, $type, $settings);

        return $field;
    }
}
